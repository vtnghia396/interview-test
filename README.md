# interview test

## Info
- Name: VO TRONG NGHIA
- Age: 24
- Gaduate: Hutech - 2018

## How to run
- Run code:
    `` npm start ``

- Data (input/output file) in './data'
- Program will read file in './data/input.txt' to get data and response result to './data/output.txt'

## How to run unit test
- Run code:
    `` npm test ``

- Test result will show in command line