var parking = require('../parking/parking.js');
var assert = require('assert');
var constant = require('../constants');

describe('Parking car', function () {
    it('Parking succesfull', function () {
        var carNumber = 'KA-01-HH-1234';
        assert.equal(parking.parking(carNumber) , 200);
    });

    it('Parking without parking lot', function () {
        var carNumber = 'KA-01-HH-1234';
        assert.equal(parking.parking(carNumber) , 200);
    });
});