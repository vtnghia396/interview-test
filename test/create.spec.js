var parking = require('../parking/create.js');
var assert = require('assert');

describe('create parking lot', function () {
    it('Create succesfull', function () {
        var capacity = 6;
        assert.equal(parking.create_parking_lot(capacity) , 200);
    });

    it('Create parking lot with capacity <= 0', function () {
        var capacity = 0;
        assert.equal(parking.create_parking_lot(capacity) , 400);
    });

    it('Create parking lot with capacity null', function () {
        var capacity = '';
        assert.equal(parking.create_parking_lot(capacity) , 404);
    });

    it('Create parking lot with capacity is string', function () {
        var capacity = '6abc';
        assert.equal(parking.create_parking_lot(capacity) , 400);
    });
});