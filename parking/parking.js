var fs = require('fs');
var constant = require('../constants');
var outputFile = constant.OUTPUT_FILE;

exports.parking =  function (carNumber) {
    if (carNumber === '') return 404;
    if (constant.PARKING_AREA.length === 0) return 400;
    var time = new Date();
    for (let i = 0; i < constant.PARKING_AREA.length; i++) {
        if (constant.PARKING_AREA[i].status === "available") {
            constant.PARKING_AREA[i].status = "unavailable";
            constant.PARKING_AREA[i].carNumber = carNumber;
            constant.PARKING_AREA[i].timeIn = time.getTime();
            fs.appendFile(outputFile, `Allocated slot number: ${constant.PARKING_AREA[i].location} \n`, function (err) {
                if (err) throw err;
            });
            break;
        }
    }
    return 200;
}