var fs = require('fs');
var constant = require('../constants');
var outputFile = constant.OUTPUT_FILE;

exports.create_parking_lot =  function (capacity) {
    if (capacity === '') return 404;
    if (isNaN(capacity)) return 400;
    if (capacity <= 0) return 400;
    fs.appendFile(outputFile, `Created parking lot with ${capacity} slots \n`, function (err) {
        if (err) throw err;
    });

    for (let i = 1; i <= capacity; i++) {
        constant.PARKING_AREA.push({
            status: 'available',
            carNumber: '',
            location: i,
            timeIn: '',
            timeOut: ''
        });
    }
    return 200;
}