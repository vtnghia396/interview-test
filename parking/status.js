var fs = require('fs');
var constant = require('../constants');
var outputFile = constant.OUTPUT_FILE;

exports.status =  function (carNumber) {
    if (constant.PARKING_AREA.length === 0) return 400;

    fs.appendFile(outputFile, `Slot No. Registration No.\n`, function (err) {
        if (err) throw err;
    });
    for (let i = 0; i < constant.PARKING_AREA.length; i++) {
        if (constant.PARKING_AREA[i].status === "unavailable") {
            fs.appendFile(outputFile, `${constant.PARKING_AREA[i].location} ${constant.PARKING_AREA[i].carNumber}\n`, function (err) {
                if (err) throw err;
            });
        }
    }
    return;
}



