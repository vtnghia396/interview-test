var fs = require('fs');
var constant = require('./constants');

var inputFile = constant.INPUT_FILE;
var outputFile = constant.OUTPUT_FILE;
var create = require('./parking/create');
var parking = require('./parking/parking');
var leave = require('./parking/leave');
var status = require('./parking/status');

fs.unlink(outputFile, function (err) {
    if (err) throw err;
});

fs.readFile(inputFile, async function (err, data) {
    if (err) {
        return console.error(err);
    }
    let arrayData = data.toString().split(/\n/);
    for (let i = 0; i < arrayData.length; i++) {
        switch (arrayData[i].split(' ')[0]) {
            case 'create_parking_lot':
                create.create_parking_lot(arrayData[i].split(' ')[1]);
                break;
            case 'park':
                parking.parking(arrayData[i].split(' ')[1])
                break;
            case 'leave':
                leave.leave(arrayData[i].split(' ')[1])
                break;
            case 'status':
                status.status()
                break;
        }
    }
});